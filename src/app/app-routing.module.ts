import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { MainLayoutComponent } from './shared/components/main-layout/main-layout.component';
import { PostPageComponent } from './post-page/post-page.component';
import { PostsResolver } from './posts/posts.resolver';
import { PostPageResolver } from './post-page/post-page.resolver';

const routes: Routes = [
  {
    path: "", component: MainLayoutComponent, children: [
      {
        path: "",
        redirectTo: "/",
        pathMatch: "full"
      },
      {
        path: "",
        component: HomePageComponent,
        resolve: { posts: PostsResolver }
      },
      {
        path: "post/:id",
        component: PostPageComponent,
        resolve: { posts: PostPageResolver }
      }
    ]
  },
  { path: "admin", loadChildren: () => import("./admin/admin.module").then(m => m.AdminModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  }),],
  exports: [RouterModule]
})
export class AppRoutingModule { }
