export interface User {
    email: string
    password: string
    returnSecureToken?: boolean
}
export interface FbAuthResponse {
    idToken: string
    expiresIn: string
}

export interface Post {
    id: string
    author: string
    title: string
    text: string
    date: Date
}

export type AlertType = "success" | "warning" | "danger"

export interface Alert {
    type: AlertType
    text: string
}