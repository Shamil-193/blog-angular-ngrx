import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { EntityActionFactory, EntityOp } from "@ngrx/data";
import { Store } from "@ngrx/store";
import { AppState } from "../store/state/app.state";

@Injectable()
export class PostsResolver implements Resolve<unknown>{

    constructor(
        private store: Store<AppState>,
        private entityActionFactory: EntityActionFactory) { }

    resolve(
    ): void {
        this.store.dispatch(
            this.entityActionFactory.create(
                "Post",
                EntityOp.QUERY_ALL
            )
        )
    }

}