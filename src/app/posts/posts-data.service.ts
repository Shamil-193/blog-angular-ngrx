import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { DefaultDataService, HttpUrlGenerator } from "@ngrx/data";
import { map, Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Post } from "../shared/interfaces/interfaces";

@Injectable()
export class PostsDataService extends DefaultDataService<Post>{
    constructor(http: HttpClient, HttpUrlGenerator: HttpUrlGenerator) {
        super("Post", http, HttpUrlGenerator)
    }

    override getAll(): Observable<Post[]> {
        return this.http.get(`${environment.fbDbUrl}/posts.json`)
            .pipe(map((response: { [key: string]: any; }) => {
                return Object
                    .keys(response)
                    .map(key => ({
                        ...response[key],
                        id: key,
                        date: new Date(response[key].date)
                    }));
            }));
    }

    override getById(id: string): Observable<Post> {
        return this.http.get<Post>(`${environment.fbDbUrl}/posts/${id}.json`)
            .pipe(map((post: Post) => {
                return {
                    ...post, id,
                    date: new Date(post.date)
                }
            }))
    }
}