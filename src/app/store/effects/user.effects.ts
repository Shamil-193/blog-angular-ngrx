import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, of, switchMap, tap } from "rxjs";
import { AuthService } from "src/app/admin/shared/services/auth.service";
import { errorList } from "src/app/admin/shared/services/error.list";
import { userLogin, userLoginFailure, userLoginSuccess, userLogout } from "../actions/user.actions";

@Injectable()
export class UserEffects {

    constructor(
        private actions$: Actions,
        private authService: AuthService,
        private router: Router
    ) { }

    userLogin$ = createEffect(() =>
        this.actions$.pipe(
            ofType(userLogin),
            switchMap(({ user }) => this.authService.login(user).pipe(
                map(() => userLoginSuccess()),
                catchError((errorMes) => {
                    const error = this.authService.getErrorMessage((errorMes.error.error.message) as errorList)
                    return of(userLoginFailure({ error }))
                })
            ))
        ))

    userRedirect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(userLoginSuccess),
            tap(() => this.router.navigate(["/admin", "dashboard"]))
        )
        , { dispatch: false })


    userLogout$ = createEffect(() =>
        this.actions$.pipe(
            ofType(userLogout),
            tap(() => {
                this.authService.logout()
                this.router.navigate(["/admin", "login"])
            })
        ), { dispatch: false }
    )
}