import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap } from "rxjs";
import { Post } from "src/app/shared/interfaces/interfaces";
import { PostsService } from "src/app/shared/posts.servise";
import { addPost, addPostSuccess, getPostById, getPostByIdSuccess, getPosts, getPostsSuccess, removePost, removePostSuccess, updatePost, updatePostSuccess } from "../actions/post.actions";

@Injectable()
export class PostsEffects {

    constructor(
        private actions$: Actions,
        private postService: PostsService
    ) { }

    getPosts$ = createEffect(() =>
        this.actions$.pipe(
            ofType(getPosts),
            switchMap(() => this.postService.getAll().pipe(
                map((posts: Post[]) => getPostsSuccess({ posts }))
            ))
        ))

    addPost$ = createEffect(() =>
        this.actions$.pipe(
            ofType(addPost),
            switchMap(({ post }) => this.postService.create(post).pipe(
                map((post: Post) => addPostSuccess({ post }))
            ))
        ))

    removePost$ = createEffect(() =>
        this.actions$.pipe(
            ofType(removePost),
            switchMap(({ id }) => this.postService.remove(id))).pipe(
                map(() => removePostSuccess())
            ))



    updatePost$ = createEffect(() =>
        this.actions$.pipe(
            ofType(updatePost),
            switchMap(({ post }) => this.postService.update(post))).pipe(
                map((post: Post) => updatePostSuccess({ post }))
            ))


    getPostById$ = createEffect(() =>
        this.actions$.pipe(
            ofType(getPostById),
            switchMap(({ id }) => this.postService.getById(id))).pipe(
                map((post: Post) => getPostByIdSuccess({ post }))
            )
    )
}