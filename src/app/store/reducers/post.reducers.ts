import { initialPostState } from "../state/posts.state";
import { addPost, addPostSuccess, getPostById, getPostByIdSuccess, getPosts, getPostsSuccess, removePost, removePostSuccess, updatePost, updatePostSuccess } from "../actions/post.actions";
import { createReducer, on } from "@ngrx/store";


export const postReducers = createReducer(
    initialPostState,
    on(getPosts, (state) => ({
        ...state,
        loading: true
    })),
    on(getPostsSuccess, (state, { posts }) => ({
        ...state,
        posts: [...posts],
        loading: false
    })),
    on(addPost, (state) => ({
        ...state,
        loading: true
    })),
    on(addPostSuccess, (state, { post }) => ({
        ...state,
        posts: [...state.posts, post],
        loading: false,
        alert: {
            type: "success",
            text: "Пост добавлен"
        }
    })),
    on(removePost, (state, { id }) => ({
        ...state,
        posts: [...state.posts.filter(post => post.id != id)],
        loading: true
    })),
    on(removePostSuccess, (state) => ({
        ...state,
        loading: false,
        alert: {
            type: "danger",
            text: "Пост удален"
        }
    })),
    on(updatePost, (state) => ({
        ...state,
        loading: true
    })),
    on(updatePostSuccess, (state, { post }) => ({
        ...state,
        posts: [...state.posts.filter(filtredPost => filtredPost != post)],
        loading: false,
        alert: {
            type: "warning",
            text: "Пост изменен"
        }
    })),
    on(getPostById, (state, { id }) => ({
        ...state,
        loading: true,
    })),
    on(getPostByIdSuccess, (state, { post }) => ({
        ...state,
        loading: false,
        currentPost: post
    })),
)