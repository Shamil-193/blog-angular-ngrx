import { createReducer, on } from "@ngrx/store";
import { userLoginFailure, userLoginSuccess, userLogout } from "../actions/user.actions";
import { initiaUserState } from "../state/users.state";

export const userReducers = createReducer(
    initiaUserState,
    on(userLoginSuccess, (state) => ({
        ...state,
        status: true,
        isAuthenticated: true
    })),
    on(userLoginFailure, (state, { error }) => ({
        ...state,
        status: true,
        error: error
    })),
    on(userLogout, (state) => ({
        ...state,
        isAuthenticated: false
    }))
)