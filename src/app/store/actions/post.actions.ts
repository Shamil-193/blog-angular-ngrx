import { createAction, props } from "@ngrx/store";
import { Post } from "src/app/shared/interfaces/interfaces";

export enum PostActiions {
    getPosts = "[Post] Get Posts",
    getPostsSuccess = "[Post] Get Post Success",
    getPostById = "[Post] Get Post By Id",
    getPostByIdSuccess = "[Post] Get Post By Id Success",
    removePost = "[Post] Remove Post",
    removePostSuccess = "[Post] Remove Post Success",
    updatePost = "[Post] Update Post",
    updatePostSuccess = "[Post] Update Post Success",
    addPost = "[Post] Add Post",
    addPostsSuccess = "[Post] Add Post Success",
}

export const getPosts = createAction(
    PostActiions.getPosts)

export const getPostsSuccess = createAction(
    PostActiions.getPostsSuccess,
    props<{ posts: Post[] }>()
)

export const getPostById = createAction(
    PostActiions.getPostById,
    props<{ id: string }>()
)

export const getPostByIdSuccess = createAction(
    PostActiions.getPostByIdSuccess,
    props<{ post: Post }>()
)

export const addPost = createAction(
    PostActiions.addPost,
    props<{ post: Post }>()
)

export const addPostSuccess = createAction(
    PostActiions.addPostsSuccess,
    props<{ post: Post }>()
)

export const removePost = createAction(
    PostActiions.removePost,
    props<{ id: string }>()
)
export const removePostSuccess = createAction(
    PostActiions.removePostSuccess
)

export const updatePost = createAction(
    PostActiions.updatePost,
    props<{ post: Post }>()
)

export const updatePostSuccess = createAction(
    PostActiions.updatePostSuccess,
    props<{ post: Post }>()
)

