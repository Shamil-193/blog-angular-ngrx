import { createAction, props } from "@ngrx/store"
import { User } from "src/app/shared/interfaces/interfaces"

export enum UserActiions {
    userLogin = "[User] User Login",
    userLoginSuccess = "[User] User Login Success",
    userLoginFailure = "[User] User Login Failure",
    userLogout = "[User] User Log",
}

export const userLogin = createAction(
    UserActiions.userLogin,
    props<{ user: User }>()
)

export const userLoginSuccess = createAction(
    UserActiions.userLoginSuccess,
)

export const userLoginFailure = createAction(
    UserActiions.userLoginSuccess,
    props<{ error: string }>()
)

export const userLogout = createAction(
    UserActiions.userLogout
)