export interface UsersState{
    status: boolean;
    error?: string;
    isAuthenticated?: boolean;
}

export const initiaUserState: UsersState ={
    status: false
}