import { PostsState } from "./posts.state";
import { UsersState } from "./users.state";

export interface AppState{
    posts: PostsState;
    users: UsersState;
}