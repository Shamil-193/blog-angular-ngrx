import { Alert, AlertType, Post } from '../../shared/interfaces/interfaces'

export interface PostsState{
    posts: Post[];
    alertStatus: AlertType | null;
    loading:boolean;
    currentPost: Post | null;
    alert?: Alert;
}

export const initialPostState: PostsState ={
    posts: [],
    alertStatus: null,
    loading: false,
    currentPost: null,
}