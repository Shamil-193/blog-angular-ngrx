import { createSelector } from "@ngrx/store"
import { AppState } from "../state/app.state"
import { PostsState } from "../state/posts.state"


export const selectPosts = (state: AppState) => state.posts


export const selectAllPosts = createSelector(
    selectPosts,
    (state: PostsState) => state.posts)

export const selectLoading = createSelector(
    selectPosts,
    (state: PostsState) => state.loading
)

export const selectCurrentPost = createSelector(
    selectPosts,
    (state: PostsState) => state.currentPost
)

export const selectAlert = createSelector(
    selectPosts,
    (state: PostsState) => state.alert
)