import { createSelector } from "@ngrx/store";
import { AppState } from "../state/app.state";
import { UsersState } from "../state/users.state";

export const selectUsers = (state: AppState) => state.users

export const selectStatus = createSelector(
    selectUsers,
    (state: UsersState) => state.status)

export const selectError = createSelector(
    selectUsers,
    (state: UsersState) => state.error);

export const isAuthenticated = createSelector(
    selectUsers,
    (state: UsersState) => state.isAuthenticated);