import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import { User } from 'src/app/shared/interfaces/interfaces';
import { userLogin } from 'src/app/store/actions/user.actions';
import { selectError } from 'src/app/store/selectors/user.selectors';
import { AppState } from 'src/app/store/state/app.state';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginPageComponent implements OnInit {

  form: FormGroup
  message: string

  public error$ = this.store.select(selectError)


  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.authStatusInit()
    this.formInit()
  }

  authStatusInit() {
    this.route.queryParams.subscribe((params: Params) => {
      if (params["loginAgain"]) {
        this.message = "Пожалуйста, войдите в профиль"
      } else if (params["authFailed"]) {
        this.message = "Сессия истекла. Авторизируйтесь повторно"
      }
    })
  }

  formInit() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    })
  }

  submit() {
    if (this.form.invalid) {
      return
    }
    const user: User = {
      email: this.form.value.email,
      password: this.form.value.password,
    }
    this.store.dispatch(userLogin({ user }))
  }


}
