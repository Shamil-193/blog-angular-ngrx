export enum errorList {
    INVALID_EMAIL = "INVALID_EMAIL",
    INVALID_PASSWORD = "INVALID_PASSWORD",
    EMAIL_NOT_FOUND = "EMAIL_NOT_FOUND",
    UNKNOWN_ERROR = ""
}

export const errorDisplay: Record<errorList, string> = {
    [errorList.INVALID_EMAIL]: "Неверный email",
    [errorList.INVALID_PASSWORD]: "Неверный пароль",
    [errorList.EMAIL_NOT_FOUND]: "Данный email не зарегистрирован",
    [errorList.UNKNOWN_ERROR]: "Неизвестная ошибка"
}