import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, Subject, tap } from "rxjs";
import { FbAuthResponse, User } from "src/app/shared/interfaces/interfaces";
import { environment } from "src/environments/environment";
import { errorDisplay, errorList } from "./error.list";


@Injectable({
    providedIn: "root"
})
export class AuthService {

    public error$: Subject<string> = new Subject<string>()

    constructor(private http: HttpClient) {
    }

    get token() {
        const expDate = new Date(localStorage.getItem("fb-token-exp")!)
        if (new Date > expDate) {
            this.logout()
            return null
        } else {
            return localStorage.getItem("fb-token")
        }
    }

    login(user: User): Observable<any> {
        user.returnSecureToken = true
        return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`, user)
            .pipe(
                tap(this.setToken as any))
    }

    getErrorMessage(error: errorList): string {
        return errorDisplay[error]
    }


    logout() {
        localStorage.clear()
    }

    isAuthenticated(): boolean {
        return !!this.token
    }

    setToken(response: FbAuthResponse) {
        const expDate = new Date(new Date().getTime() + +response.expiresIn * 1000)
        localStorage.setItem("fb-token", response.idToken)
        localStorage.setItem("fb-token-exp", expDate.toString())
    }

}