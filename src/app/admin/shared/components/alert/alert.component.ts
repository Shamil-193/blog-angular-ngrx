import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AlertType } from 'src/app/shared/interfaces/interfaces';
import { selectAlert } from 'src/app/store/selectors/post.selectors';
import { AppState } from 'src/app/store/state/app.state';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlertComponent implements OnInit, OnDestroy {

  @Input() delay = 3000

  public text: string | null
  public type: AlertType
  aSub: Subscription

  constructor(private store: Store<AppState>,
    private detectChanges: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.aSub = this.store.select(selectAlert).subscribe(alert => {
      this.text = alert?.text!
      this.type = alert?.type!
      const timeout = setTimeout(() => {
        clearTimeout(timeout)
        this.text = ""
        this.detectChanges.detectChanges()
      }, this.delay)
      this.detectChanges.detectChanges()
    })
  }

  ngOnDestroy(): void {
    if (this.aSub) {
      this.aSub.unsubscribe()
    }
  }

}
