import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { userLogout } from 'src/app/store/actions/user.actions';
import { isAuthenticated } from 'src/app/store/selectors/user.selectors';
import { AppState } from 'src/app/store/state/app.state';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminLayoutComponent {

  public isAuthenticated = this.store.select(isAuthenticated)

  constructor(private store: Store<AppState>) {}

  logOut(event: Event) {
    event.preventDefault()
    this.store.dispatch(userLogout())
  }

}
