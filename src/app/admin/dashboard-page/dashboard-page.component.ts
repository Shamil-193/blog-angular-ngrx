import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { removePost } from 'src/app/store/actions/post.actions';
import { selectAllPosts, selectLoading } from 'src/app/store/selectors/post.selectors';
import { AppState } from 'src/app/store/state/app.state';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardPageComponent {

  public posts$ = this.store.select(selectAllPosts)
  public loading$ = this.store.select(selectLoading)

  searchStr = ""

  constructor(private store: Store<AppState>) { }

  remove(id: string) {
    this.store.dispatch(removePost({ id }))
  }

}
