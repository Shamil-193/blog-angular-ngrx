import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from "@angular/material/button";

import { LoginPageComponent } from "./login-page/login-page.component";
import { AdminLayoutComponent } from "./shared/components/admin-layout/admin-layout.component";
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { CreatePageComponent } from './create-page/create-page.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { SharedModule } from "../shared/shared.module";
import { AuthGuard } from "./shared/services/auth.guard";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { SearchPipe } from "./shared/search.pipe";
import { AlertComponent } from './shared/components/alert/alert.component';
import { RouterAdminModule } from "./router.admin.module";
import { DashboardPageResolver } from "./resolvers/dashboard-page.resolver";
import { EditPageResolver } from "./resolvers/edit-page.resolver";

@NgModule({
    declarations: [
        AdminLayoutComponent,
        LoginPageComponent,
        DashboardPageComponent,
        CreatePageComponent,
        EditPageComponent,
        SearchPipe,
        AlertComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        ReactiveFormsModule,
        MatButtonModule,
        RouterAdminModule
    ],
    providers: [
        AuthGuard,
        DashboardPageResolver,
        EditPageResolver,
    ],
    exports: [RouterModule],
    schemas: []
})
export class AdminModule {

}