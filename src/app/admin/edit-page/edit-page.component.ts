import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Post } from 'src/app/shared/interfaces/interfaces';
import { updatePost } from 'src/app/store/actions/post.actions';
import { selectCurrentPost, selectLoading } from 'src/app/store/selectors/post.selectors';
import { AppState } from 'src/app/store/state/app.state';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditPageComponent implements OnInit {

  form: FormGroup
  post: Post

  public loading$ = this.store.select(selectLoading)
  public currentPost$ = this.store.select(selectCurrentPost).subscribe(post => {
    if (post) {
      this.post = post
      this.form.setValue({
        title: post!.title,
        text: post!.text
      })
    }
  })

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.formInit()
  }

  formInit() {
    console.log("form init")
    this.form = new FormGroup({
      title: new FormControl("", [Validators.required, Validators.minLength(3)]),
      text: new FormControl("", Validators.required)
    })
  }

  submit() {
    if (this.form.invalid) {
      return
    }

    this.store.dispatch(updatePost(
      {
        post: {
          ...this.post,
          title: this.form.value.title,
          text: this.form.value.text
        }
      }))
  }
}
