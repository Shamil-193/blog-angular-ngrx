import { Injectable } from "@angular/core"
import { Resolve } from "@angular/router"
import { Store } from "@ngrx/store"
import { map, Observable } from "rxjs"
import { getPosts } from "src/app/store/actions/post.actions"
import { selectAllPosts } from "src/app/store/selectors/post.selectors"
import { AppState } from "src/app/store/state/app.state"

@Injectable()
export class DashboardPageResolver implements Resolve<boolean>{

    constructor(private store: Store<AppState>) { }

    resolve(
    ): Observable<boolean> {
        return this.store.select(selectAllPosts).pipe(
            map(posts => {
                if (posts.length === 0) {
                    this.store.dispatch(getPosts())
                    return true
                } else {
                    return false
                }
            })
        )
    }

}