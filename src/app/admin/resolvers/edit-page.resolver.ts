import { Injectable } from "@angular/core"
import { ActivatedRouteSnapshot, Resolve } from "@angular/router"
import { Store } from "@ngrx/store"
import { getPostById } from "src/app/store/actions/post.actions"
import { AppState } from "src/app/store/state/app.state"

@Injectable()
export class EditPageResolver implements Resolve<boolean>{

    constructor(
        private store: Store<AppState>,
    ) {}

    resolve(route: ActivatedRouteSnapshot): boolean {
        this.store.dispatch(getPostById({ id: route.params.id }))
        return true
    }

}