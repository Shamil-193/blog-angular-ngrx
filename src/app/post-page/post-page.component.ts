import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EntitySelectorsFactory } from '@ngrx/data';
import { Dictionary } from '@ngrx/entity';
import { select, Store } from '@ngrx/store';
import { filter, first, map, Observable, switchMap } from 'rxjs';
import { Post } from '../shared/interfaces/interfaces';
import { AppState } from '../store/state/app.state';


@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostPageComponent {

  post$: Observable<Post> = this.store.pipe(
    select(
      this.entutySelectorsFactory.create<Post>("Post").selectEntityMap
    ),
    filter((entities: Dictionary<Post>) => !!Object.keys(entities)?.length),
    switchMap((entities: Dictionary<Post>) => this.route.params.pipe(
      first(),
      map((params: Params) => entities[params.id]!)
    ))
  )


  constructor(
    private store: Store<AppState>,
    private entutySelectorsFactory: EntitySelectorsFactory,
    private route: ActivatedRoute
  ) { }

}
