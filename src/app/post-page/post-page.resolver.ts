import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve } from "@angular/router";
import { EntityActionFactory, EntityOp } from "@ngrx/data";
import { Store } from "@ngrx/store";
import { AppState } from "../store/state/app.state";

@Injectable()
export class PostPageResolver implements Resolve<boolean>{

    constructor(
        private store: Store<AppState>,
        private entityActionFactory: EntityActionFactory,
    ) { }

    resolve(route: ActivatedRouteSnapshot): boolean {
        this.store.dispatch(
            this.entityActionFactory.create(
                "Post",
                EntityOp.QUERY_BY_KEY,
                route.params.id
            )
        )
        return true
    }
}