import { ChangeDetectionStrategy, Component } from '@angular/core';
import { EntitySelectorsFactory } from '@ngrx/data';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Post } from '../shared/interfaces/interfaces';
import { AppState } from '../store/state/app.state';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePageComponent {


  public posts$: Observable<Post[]> = this.store.select(this.entutySelectorsFactory.create<Post>("Post").selectEntities)

  constructor(
    private store: Store<AppState>,
    private entutySelectorsFactory: EntitySelectorsFactory
  ) { }

}
