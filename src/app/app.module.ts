import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule, Provider} from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import frenchLocale from '@angular/common/locales/ru';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MainLayoutComponent} from './shared/components/main-layout/main-layout.component';
import {HomePageComponent} from './home-page/home-page.component';
import {PostPageComponent} from './post-page/post-page.component';
import {PostComponent} from './shared/components/post/post.component';
import {SharedModule} from './shared/shared.module';
import {AuthInterceptor} from './shared/auth.interceptor';
import {MatTableModule} from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { StoreModule } from '@ngrx/store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { PostsEffects } from './store/effects/post.effects';
import { EntityDataModule, EntityDataService } from '@ngrx/data';
import { entityConfig } from './entity-metadata';
import { postReducers } from './store/reducers/post.reducers';
import { userReducers } from './store/reducers/user.reducer';
import { UserEffects } from './store/effects/user.effects';
import { PostsResolver } from './posts/posts.resolver';
import { PostsDataService } from './posts/posts-data.service';
import { PostPageResolver } from './post-page/post-page.resolver';



registerLocaleData(frenchLocale)

const INTERCEPTOR_PROVIDER: Provider={
  provide: HTTP_INTERCEPTORS, 
  multi: true,
  useClass: AuthInterceptor
}

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    HomePageComponent,
    PostPageComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatButtonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    StoreModule.forRoot({posts: postReducers, users: userReducers},{
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      },
    }),
    EffectsModule.forRoot([PostsEffects, UserEffects]),
    StoreRouterConnectingModule.forRoot(),
    EntityDataModule.forRoot(entityConfig)
  ],
  providers: [INTERCEPTOR_PROVIDER,
    {provide: LOCALE_ID, useValue: 'ru'}, 
    PostsDataService,
    PostsResolver,
    PostPageResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    entityDataService: EntityDataService,
    postsDataService: PostsDataService
  ){
    entityDataService.registerService("Post", postsDataService)
  }
}
